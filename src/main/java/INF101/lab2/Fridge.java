package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int max_size = 20;

    // FridgeItem[] fridgeArray = new Fridgeitem[20];
    ArrayList<FridgeItem> fridgeArray = new ArrayList<FridgeItem>(20);

    @Override
    public int totalSize() {
        // GOOD
        return max_size;

    }
    @Override
    public int nItemsInFridge() {
        // int itemCount = 0;
        // for (int i = 0; i < fridgeArray.size(); i++) {
        //     if (fridgeArray != null) {
        //         itemCount++;
        //     }
        // }
        // return itemCount;

        return fridgeArray.size();

    }
    @Override
    public boolean placeIn(FridgeItem item) {
        // fridgeArray.add(item)
        if (totalSize() == nItemsInFridge()) {
            return false;
        }
        else {
            fridgeArray.add(item);

        return true;
        }
        
        }
    
    @Override
    public void takeOut(FridgeItem item) {
        if (!fridgeArray.contains(item)) {
            throw new NoSuchElementException();
        }
        else {
            fridgeArray.remove(item);
        }
        
    }
    @Override
    public void emptyFridge() {
        // GOOD
        if (fridgeArray != null) {
          fridgeArray.clear();
        }
        

    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub

        // ArrayList<FridgeItem> expiredfood = new ArrayList<>();
        // for (int i=0; i < nItemsInFridge(); i++) {
        //     FridgeItem item = items.get(i);
        //     if (item.hasExpired()) {
        //         expiredfood.add(item);
        //     }
        // }
        // for (FridgeItem expiItem : expiredfood) {
        //     items.remove(expiItem);
        // }
        // return expiredfood;

        ArrayList<FridgeItem> badFood = new ArrayList<>();
        for (FridgeItem item: fridgeArray) {
            if (item.hasExpired()) {
                badFood.add(item);
                String itemNameitem = item.getName();
                System.out.println(itemNameitem);

            }
        }
        fridgeArray.removeAll(badFood);
        return badFood;
        }
        
    }
    

